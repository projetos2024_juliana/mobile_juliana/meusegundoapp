import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function LayoutGrade() {
  return (
    <View style={styles.container}>
      <View style={styles.linha}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
      </View>
      <View style={styles.linha}>
        <View style={styles.box3}></View>
        <View style={styles.box4}></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FAC0D0"
  },
  linha: {
    flexDirection: 'row'    //horizontal
  },
  box1: {
    width: 60,
    height: 60,
    backgroundColor: "#D4063A"
  },
  box2: {
    width: 60,
    height: 60,
    backgroundColor: "#F47494"
  },
  box3: {
    width: 60,
    height: 60,
    backgroundColor: "#EA2E5D"
  },
  box4: {
    width: 60,
    height: 60,
    backgroundColor: "#F49AB2"
  }
});
