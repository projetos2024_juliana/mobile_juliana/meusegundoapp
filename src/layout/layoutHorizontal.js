import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function LayoutHorizontal() {
  return (
    <View style={styles.container}>
      <ScrollView horizontal={true}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row", //FlexDirection: "column"
  },
  box1: {
    width: 85,
    height: 85,
    borderRadius: 50,
    backgroundColor: "#D4063A",
  },
  box2: {
    width: 85,
    height: 85,
    borderRadius: 50,
    backgroundColor: "#F47494",
  },
  box3: {
    width: 85,
    height: 85,
    borderRadius: 50,
    backgroundColor: "#EA2E5D",
  },
});
