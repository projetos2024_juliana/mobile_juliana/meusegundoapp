import { StyleSheet, Text, View, Button } from "react-native";

function SegundaTela({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>Segunda Tela</Text>
      <Button title="Segunda Tela" onPress={() => navigation.goBack()}></Button>
    </View>
  );
}

export default SegundaTela;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
