import { StyleSheet, Text, View, Button } from "react-native";

function TelaInicial({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>Tela inicial</Text>
      <Button
        title="Ir para a segunda tela"
        onPress={() => navigation.navigate("SegundaTela")}
      ></Button>
    </View>
  );
}

export default TelaInicial;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
