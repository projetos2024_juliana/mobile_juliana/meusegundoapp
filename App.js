import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import Layout from "./src/layout/layoutDeTelaEstrutura";
import LayoutHorizontal from './src/layout/layoutHorizontal';
import LayoutGrade from './src/layout/layoutGrade';
import Components from './src/components';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SegundaTela from './src/segundatela';
import TelaInicial from './src/telainicial';
import Menu from './src/menu';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>

        <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="TelaInicial" component={TelaInicial}/>
        <Stack.Screen name="SegundaTela" component={SegundaTela}/>
          
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  

});